#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Unpaid CSV.
A script for Hackerspace.gr to remind members of their late subscription fees.
"""

#    Copyright (C) 2016  Sotirios Vrachas <sotirio@vrachas.net>
#    Copyright (C) 2020  Agis Zisimatos <agzisim@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from csv import reader
from datetime import date, datetime
from string import Template
from smtplib import SMTP
from email.mime.text import MIMEText
from email.header import Header
from email_template import SUBJECT, BODY
import config


def send_email(to_addr, subject, body):
    """Sends Email"""
    msg = MIMEText(body.encode('utf-8'), _charset='utf-8')
    msg['Subject'] = Header(subject, 'utf-8')
    msg['From'] = config.EMAIL_FROM
    msg['To'] = to_addr
    if config.EMAIL_METHOD == 'smtp':
        server = SMTP(config.SMTP_SERVER)
        if config.SMTP_SECURETY == 'starttls':
            server.starttls()
        if config.SMTP_AUTH:
            server.login(config.SMTP_USERNAME, config.SMTP_PASSWORD)
        server.sendmail(config.EMAIL_FROM, [to_addr], msg.as_string())
        server.quit()
    else:
        print(msg.as_string())


def mail_loop(members):
    """Loop over members. Calls send_email if fees are due"""
    for member in members:
        name = member[0]
        to_addr = member[1]
        to_paid = float(member[2])
        due_month = int(member[3])
        tier = float(member[4])
        if (to_paid >= 0):
            phrase = 'Δεν οφείλεται τίποτα μέχρι και τον %dο μήνα' \
                     ' και έχετε δώσει επιπλέον %.1f€. Η μηνιαία δωρεά' \
                     ' σας είναι %.1f€' % (due_month, to_paid, tier)
        else:
            phrase = 'Η οφειλή σας μέχρι και τον %dο μήνα είναι %.1f€.' \
                     'Η μηνιαία δωρεά σας είναι %.1f€' % (due_month, -to_paid, tier)
        subject = Template(SUBJECT).substitute(name=name, to_addr=to_addr,
                                               to_paid=to_paid,
                                               phrase=phrase,
                                               due_month=due_month)
        body = Template(BODY).substitute(name=name, to_addr=to_addr,
                                        to_paid=to_paid,
                                        phrase=phrase,
                                        due_month=due_month)
        send_email(to_addr, subject, body)


with open(config.FILENAME, 'rt') as csvfile:
    MEMBERS = reader(csvfile, delimiter=',')
    mail_loop(MEMBERS)
