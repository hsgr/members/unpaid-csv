A script for Hackerspace.gr to remind members of their late subscription fees.

It is fork from [hsgr/unpaid-csv/](https://github.com/hsgr/unpaid-csv/).

## Getting Started
```
git clone https://gitlab.com/zisi/unpaid-csv
cd unpaid-csv
```
Produce a members.csv file with LibreOffice Calc or similar. See included members-example for the appropriate format.

```
cp config_example.py config.py
vi config.py
vi email_template.py
python3 unpaid.py
```
The .csv file must contain: name,user@example.com,to_be_paid, due_to_month, tier

## Documentation

## Support & Community

## Contribution guidelines
Use Pylint3

## Wanted features

## Contributor list
Sotirios Vrachas
Pierros Papadeas
Nikos Roussos
Agis Zisimatos

## License
AGPL v3

## Credits, Inspiration, Alternatives
Hackerspace.gr
